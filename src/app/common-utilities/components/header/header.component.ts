import { constHeader } from "./../../constants/header.constants";
import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { Header } from "src/app/common-utilities/book-store-models/Header.model";
import { Observable, Subscription } from "rxjs";
import { UserState } from "../../store/states/user.state";
import { Select, Store } from "@ngxs/store";
import { CartState } from "../../store/states/cart.state";
import { BookStoreCommonService } from "../../services/book-store-common.service";
import { SetCartCount } from "../../store/actions/cart.action";

@Component({
    selector: "app-header",
    templateUrl: "./header.component.html",
    styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit, OnDestroy {
    @Input() sidenav;
    header: Header = new Header();
    @Input() cart;
    cartItemsCount: number = 0;
    subscription: Subscription = new Subscription();
    @Select(UserState.getUserId) userId: Observable<number>;
    @Select(CartState.getCartCount) cartCount: Observable<number>;
    user: number;

    constructor(private commonService: BookStoreCommonService, private store: Store) {}

    /**
     * gets cart items and count of cartItems after user logins
     */
    ngOnInit(): void {
        this.getHeaderData();
        this.userId.subscribe((user) => {
            this.user = user;
            if (user !== 0) {
                this.getCartItems();
            }
        });
        this.cartCount.subscribe((count) => (this.cartItemsCount = count));
    }

    /**
     * Gets header data
     */
    getHeaderData(): void {
        this.header = constHeader;
    }

    /**
     * Gets cart items
     */
    getCartItems() {
        this.subscription = this.commonService.getCartItems(this.user).subscribe((data) => {
            this.cartItemsCount = data.length;
            this.store.dispatch(new SetCartCount(this.cartItemsCount));
        });
    }

    /**
     * unsubscribes all the api calls subscribed on destroy
     */
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
