import { Injectable } from "@angular/core";
import { MatPaginator, MatPaginatorIntl } from "@angular/material/paginator";

@Injectable()
export class CustomPaginator extends MatPaginatorIntl {
    constructor() {
        super();
        this.getRangeLabel = (page: number, pageSize: number, length: number) => "";
    }
}
