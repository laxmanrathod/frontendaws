export const featuredCategories = [
    {
        icon: "collections",
        category_name: "Arts & Photography",
        content: "Shop Now",
        color_background: "#fff6f6",
        color_icon: "#db82ae",
        id: 1,
    },
    {
        icon: "fastfood",
        category_name: "Food & Drink",
        content: "Shop Now",
        color_background: "#f4e6e5",
        color_icon: "#e9b24d",
        id: 2,
    },
    {
        icon: "favorite",
        category_name: "Romance",
        content: "Shop Now",
        color_background: "#faf1ff",
        color_icon: "#f75454",
        id: 3,
    },
    {
        icon: "description",
        category_name: "Biography",
        content: "Shop Now",
        color_background: "#ffedcb",
        color_icon: "#f06735",
        id: 4,
    },
    {
        icon: "health_and_safety",
        category_name: "Health",
        content: "Shop Now",
        color_background: "#faf1ff",
        color_icon: "#6b6bf3",
        id: 5,
    },
];
