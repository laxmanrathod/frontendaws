import { routes } from "./../../../app-routing.module";
import { CommonModule } from "@angular/common";
import { UserState } from "./../../../common-utilities/store/states/user.state";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { ComponentFixture, fakeAsync, TestBed, tick } from "@angular/core/testing";
import { LoginComponent } from "./login.component";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { ErrorMessages } from "../../../common-utilities/constants/errors.enum";
import { NgxsModule, Store } from "@ngxs/store";
import { BookStoreCommonService } from "../../../common-utilities/services/book-store-common.service";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { User } from "src/app/common-utilities/book-store-models/User.model";

describe("LoginComponent", () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let service: BookStoreCommonService;
    let httpMock: HttpTestingController;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [LoginComponent],
            imports: [
                CommonModule,
                MatFormFieldModule,
                MatInputModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatSnackBarModule,
                BrowserAnimationsModule,
                HttpClientModule,
                NgxsModule.forRoot([UserState]),
                RouterTestingModule.withRoutes(routes),
                HttpClientTestingModule,
            ],
            providers: [BookStoreCommonService],
        }).compileComponents();
    });
    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        service = TestBed.inject(BookStoreCommonService);
        httpMock = TestBed.inject(HttpTestingController);
        fixture.detectChanges();
    });

    afterEach(() => {
        httpMock.verify();
    });
    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should initialize login Form", () => {
        const login = {
            username: "",
            password: "",
        };
        expect(component.login.value).toEqual(login);
    });
    it("Should invalidate the form", () => {
        component.login.controls.username.setValue("");
        component.login.controls.password.setValue("");
        expect(component.login.valid).toBeFalsy();
    });

    it("Should validate the form", () => {
        component.login.controls.username.setValue("bookstore_1");
        component.login.controls.password.setValue("Test@12345");
        expect(component.login.valid).toBeTruthy();
    });

    it("needs to call onSubmit() when user click on button", fakeAsync(() => {
        jest.spyOn(component, "onSubmit");
        let button = fixture.debugElement.nativeElement.querySelector(".btn");
        button.click();
        tick();
        expect(component.onSubmit).toHaveBeenCalled();
    }));

    it("service should be created", () => {
        const service: BookStoreCommonService = TestBed.inject(BookStoreCommonService);
        expect(service).toBeTruthy();
    });

    it("Should login and return data", fakeAsync(() => {
        const user = {
            username: "Ankush75",
            password: "Ankush123",
        } as User;
        service.login(user).subscribe((data) => {
            tick();
            expect(data).toEqual({ msg: "success" });
        });
        const req = httpMock.expectOne("/api/user/login");
        expect(req.request.method).toBe("POST");
        req.flush({ msg: "success" });
    }));
});
